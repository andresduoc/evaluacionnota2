Guía para web Restaurant Mar Azul

Funcionalidades Usuario Administrador
Para poder ingresar a todas las funcionalidades del sistema, primero se debe logear como administrador con las credenciales:
•	admin
•	admin
(Minúsculas todo)

y esto desplegará unas nuevas funcionalidades en la barra de navegación donde el administrador del sistema podrá hacer uso de las siguientes funcionalidades: 
“Formulario, Listar (Eliminar y Modificar), Subir Imagen, Filtros de búsqueda”. 
•	API: listará a todas las personas, se podrá agregar y listar url “http://localhost:8000/api/personas/”.


Funcionalidades Usuario Normal

Los usuarios nuevos, que se registren en la web, tendrán la posibilidad de llenar un formulario para la reserva de una mesa según la opción que el usuario necesite (desayuno, cena y once) y podrán visualizar solo algunas funcionalidades ya que no tiene permiso de administrador.
Las funcionalidades que vera serán: Formulario, Menus,Inicio y por defecto (inicio de sección y registro de usuario)

El usuario que no se registre no podrá llenar formulario de reservación.




