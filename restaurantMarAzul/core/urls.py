
from django.urls import path, include
from .views import home, menu,listado_persona,nueva_persona,modificar_persona,eliminar_persona,registro_usuario,carga_imagen,buscar,PersonaViewSet
from rest_framework import routers

#api
#creo una variable router nos entrega el enrutador por defecto 
# al cual nosostro le vamos a agregar cosas a la variable
router = routers.DefaultRouter()
# a  la variable router le vamos a registrar una url llamada personas
# y va estar ligada al personaViewset aca definimos el get y el post
#osea genera las url get y post
router.register('personas', PersonaViewSet)


urlpatterns = [
    path('', home ,name="home"),
    path('menu/', menu ,name="menu"),
    path('listado-persona/', listado_persona ,name="listado_persona"),
    path('nueva-persona/', nueva_persona ,name="nueva_persona"),
    #le paso el id para modificar a la persona y se lo manda el views
    path('modificar-persona/(<id>/', modificar_persona ,name="modificar_persona"),
    #le paso el id para eliminar a la persona y se lo manda el views
    path('eliminar-pelicula/<id>/', eliminar_persona ,name="eliminar_persona"),
    path('registro/', registro_usuario ,name="registro_usuario"),
    path('imagen/',carga_imagen ,name="imagen"),
    path('buscar/',buscar ,name="bucarrut"),
    # incluimos del rourt sus url que va a general arriba el get y el post
    path('api/', include(router.urls)),

]