from django.contrib import admin
from .models import Persona, tipoServicio


# Register your models here.

class PersonaAdmin(admin.ModelAdmin):
    list_display = ['rut','nombre','cantidad_de_personas','correo_electronico','servicio']
    search_fields = ['rut','nombre']
    list_per_page = 1

 

admin.site.register(tipoServicio)
admin.site.register(Persona ,PersonaAdmin)




