from django.db import models

# Create your models here.

class tipoServicio(models.Model):
    nombre = models.CharField(max_length=50)

    
    def __str__(self):
        return self.nombre

class Persona(models.Model):
    #crea un id AUTOINCREMENTABLE
    rut = models.CharField(max_length=80)
    nombre = models.CharField(max_length=50)
    cantidad_de_personas = models.IntegerField()
    correo_electronico = models.EmailField(max_length=50)
    servicio = models.ForeignKey(tipoServicio, on_delete=models.CASCADE)

    def __str__(self):
        return self.nombre


class Imagen(models.Model):
    
    imagen = models.ImageField(null=True, blank=True)



