from django import forms
# permite hacer formularios asociados a un modelo de db
from django.forms import ModelForm
# el modelo que quiero hacer el formulario html
from .models import Persona, Imagen
# el formulario lo trae hecho django para crear un usuario
from django.contrib.auth.forms import UserCreationForm
# importo el user 
from django.contrib.auth.models import User 

#creo formulario hereda del modelform 

class PersonaForm(ModelForm):

    class Meta:
        #el modelo asociados es persona para saber guardarlos en la bd
        model = Persona
        #que campos va a tener el formulario
        #django toma este formulario y lo converte en html automaticamente 
        fields =['rut','nombre','cantidad_de_personas','correo_electronico','servicio']


# para personalizar el registro de usuario y heredamos del usercreateform
class CustomUserForm(UserCreationForm):
    
    class Meta:
        model = User 
        fields = ['first_name', 'last_name', 'email', 'username', 'password1', 'password2']


class ImagenForm(ModelForm):

    class Meta:
        model = Imagen
        fields = ['imagen']




        