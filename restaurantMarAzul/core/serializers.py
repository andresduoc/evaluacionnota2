from rest_framework import serializers
from .models import Persona

#hacelos una clases y se hereda de moldelSerializer
#meta definimos el modelo que vamos a serializar
# los campos que vamos a mostrar
class PersonaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Persona
        fields = ['rut','nombre','cantidad_de_personas','correo_electronico','servicio']


        