from django.shortcuts import render,redirect
from .models import Persona, Imagen
# importo el personaForm la clase que hace el formulario
from .forms import PersonaForm, CustomUserForm, ImagenForm
# para usurios que esten logiados / solo los usuarios que tiene ese permiso
from django.contrib.auth.decorators import login_required ,permission_required
# con estas dos funciones autentico al usuario y hago un login para redirigirlo
from django.contrib.auth import login,authenticate
#api 
# views es una vista resive los dato y hace la consulta a la bd
from rest_framework import viewsets
from .serializers import PersonaSerializer

# Create your views here.

def home(request):
    return render(request, 'core/home.html')

def menu(request):
    data = {
        'imagenes':Imagen.objects.all()
    }
    return render(request, 'core/menu.html',data)





def listado_persona(request):
    #creo una variable para contener el arreglo llamo al modelo y llamo a objects 
    # este atributo tiene acceso a la base de dato y llamo atodas la personas
    personas = Persona.objects.all()
    # en este diccionario paso las personas al template
    # y se pasa por tercer parametro para que se envie al template
    data = {
        'personaListar' : personas
    }
    return render(request, 'core/listado_persona.html',data)

#@permission_required('core.add_persona')
def nueva_persona(request):
    #creo una instancia de personaForm vacia para que 
    #el usuario rellene los datos y lo paso como tercer parametro para que lo muestre
    data = {
        'form':PersonaForm()
    }
    # si el metodos es post
    if request.method == 'POST':
        #creo un formulario todos los datos que el usuario escribio en el formulario  
        formulario = PersonaForm(request.POST)
        # formulario cumple con todas la validaciones 
        if formulario.is_valid():
            formulario.save()

    
    return render(request, 'core/nueva_persona.html',data)

 # le paso el id para modificar a la persona
def modificar_persona(request, id):
     #creo una variable para contener el arreglo llamo al modelo y llamo a objects 
    # este atributo tiene acceso a la bd y lo llamo con el get con el id que lo paso por parametro el id a modificar
    personass = Persona.objects.get(id=id)
     #creo una instancia de personaForm() vacia y 
     # lo relleno con la variable que hice personass que voy a modificar con el id a modificar
    data = {
        'form': PersonaForm(instance=personass)
    }
    # si el metodos es post
    if request.method =='POST':
        # creo una variable formulario  si la data es post y despues modifico
        #la instancia llamada personass que esta mas arriba
        formulario = PersonaForm(data=request.POST, instance=personass)
        # formulario cumple con todas la validaciones 
        if formulario.is_valid():
            
            formulario.save()
            # sobre escribo la variable form y le paso el nuevo formulario actualizado en los texto
            data['mensaje'] = "Modificado Correctamente"
            data['form']= formulario


           
    return render(request, 'core/modificar_persona.html',data)

 # le paso el id para eliminar  a la persona
def eliminar_persona(request, id):
     #creo una variable para contener el arreglo llamo al modelo y llamo a objects 
    # este atributo tiene acceso a la bd y lo llamo con el get con el id que lo paso por parametro el id a modificar
    personax = Persona.objects.get(id=id)
    #cuando la traiga la elimina
    personax.delete()
    
     
    return redirect(to="listado_persona")


def registro_usuario(request):
    #le envio el formulario que traer django por defecto al templates en un diccionario
    data = {
        'form': CustomUserForm()
    }

    if request.method == 'POST':
        formulario = CustomUserForm(request.POST)
        if formulario.is_valid():
            formulario.save()
             # toda esta funcion se registra y se autentica al usuario y lo redirige  al inicio
             # obtener los datos del clientes en el formulario
            username = formulario.cleaned_data['username'] #datos limpios
            password = formulario.cleaned_data['password1']
             # me devuelve al usuario autenticado 
            user = authenticate(username=username, password=password)
             #logiamos al usuario y le pasamos al usuario para autenticarlo en el sistema
            login(request, user)
            return redirect(to='home')
  
    return render(request, 'registration/registrar.html',data)



def carga_imagen(request):
    data = {
        'form':ImagenForm()
    }
    if request.method == 'POST':
        formulario = ImagenForm(request.POST, files=request.FILES)
        if formulario.is_valid():
            formulario.save()

    
    return render(request, 'core/imagen.html', data)




def buscar(request):
    personas = Persona.objects.all()
    cantidad_de_personas = 0  # Filtro por defecto

    if request.POST.get('cantidad_de_personas'):
        cantidad_de_personas = int(request.POST.get('cantidad_de_personas'))
        personas = personas.filter(cantidad_de_personas__gte=cantidad_de_personas)

    return render(request, "core/buscar.html", {'personas': personas, 'cantidad_de_personas':cantidad_de_personas})
    
   
# resive los datos y tambien se encarga de hacer la consulta
# la clase serializador que se encarga de pasar a json es PersonaSerializer
# 
class PersonaViewSet(viewsets.ModelViewSet):
    queryset = Persona.objects.all()
    serializer_class = PersonaSerializer

# guarda y devuelva el json desde la bd